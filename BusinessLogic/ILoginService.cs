using System;
using Domain;
public interface ILoginService{
	bool SignIn(User u);
}